import React, { useState,useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import { Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

import {UserProvider} from './userContext.js';

import AppNavbar from './components/AppNavBar.js';
/*import ProductView from './pages/ProductView.js'*/
import Home from './pages/Home.js';
import Products from './pages/Products.js';
import Register from './pages/Register.js';
import LogIn from './pages/Login.js';
import LogOut from './pages/Logout.js';
import Error from './pages/Error.js';

export default function App(){
	const [ user, setUser ] = useState( { 
		id: null,
		isAdmin: null
	} );

	const unsetUser = () =>{
		localStorage.clear();

		setUser({
			id: null,
			isAdmin: null
		});
	};

	useEffect(()=>{
		fetch(`https://immense-refuge-40846.herokuapp.com/api/users/details`,{
			method: 'GET',
			headers:{
				Authorization: `Token: ${localStorage.getItem('access')}`
			}
		}).then(res=>res.json())
		.then(data=>{
			console.log(data.isAdmin)
			if(typeof data._id !== "undefined"){
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			}else{
				setUser({
					id: null,
					isAdmin: null
				})
			}
		})
	},[]);



	return(
		<UserProvider value={{user,setUser,unsetUser}}>
			<Router>
				<AppNavbar/>
				<Routes>
					<Route path = "/" element={<Home />} />
					<Route path = "/products" element={<Products />} />
					<Route path = "/register" element={<Register />} />
					<Route path = "/login" element={<LogIn />} />
					<Route path = "/logout" element={<LogOut />} />
					<Route path = "*" element={<Error />} />
				</Routes>
			</Router>
		</UserProvider>
		)
}