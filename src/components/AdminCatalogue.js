import { Fragment, useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminCatalogue(props){

	const { productsData, fetchData } = props;

	const [productId, setProductId] = useState("");
	const [products, setProducts] = useState([]);
	const [productName, setName] = useState("");
	const [quantity, setQuantity] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);

	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	const openEdit = (productId) => {

		// Fetches the selected item using the product ID
		fetch(`https://immense-refuge-40846.herokuapp.com/api/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			setProductId(data._id);
			setName(data.productName);
			setDescription(data.description);
			setQuantity(data.quantity);
			setPrice(data.price);
		})

		setShowEdit(true);

	};

	const closeEdit = () => {

		setShowEdit(false);
		setName("");
		setDescription("");
		setPrice(0);

	};

	const addProduct = (e) => {

		// Prevent the form from redirecting to a different page on submit 
		// Helps retain the data if adding a course is unsuccessful
		e.preventDefault();

		fetch(`http://immense-refuge-40846.herokuapp.com/api/products/createProduct`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('access') }`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				quantity: quantity,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true) {
				fetchData();
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully added."					
				})
				setName("")
				setDescription("")
				setQuantity(0);
				setPrice(0)
				closeAdd();

			} else {
				console.log(data)
				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}


	const editProduct = (e, productId) => {
		
		e.preventDefault();

		fetch(`https://immense-refuge-40846.herokuapp.com/api/products/${ productId }`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('access') }`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				quantity: quantity,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {

				fetchData();

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully updated."
				});

				closeEdit();

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}

		})
	}

	useEffect(() => {

		const archiveToggle = (productId, isActive) => {

			console.log(!isActive);

			fetch(`https://immense-refuge-40846.herokuapp.com/api/products/${ productId }/archive`, {
				method: 'PUT',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('access') }`
				},
				body: JSON.stringify({
					isActive: !isActive
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if (data === true) {

					fetchData();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Actions on product successful."
					});

				} else {

					fetchData();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		}

		const productsArr = productsData.map(product => {

			return(

				<tr key={product._id}>
					<td>{product.productName}</td>
					<td>{product.description}</td>
					<td>{product.quantity}</td>
					<td>{product.price}</td>
					<td>

						{product.isActive
							? <span>Available</span>
							: <span>Unavailable</span>
						}
					</td>
					<td>
						<Button
							variant="primary"
							size="sm"
							onClick={() => openEdit(product._id)}
						>
							Update
						</Button>
						{product.isActive
							?
							<Button 
								variant="danger" 
								size="sm" 
								onClick={() => archiveToggle(product._id, product.isActive)}
							>
								Disable
							</Button>
							:
							<Button 
								variant="success"
								size="sm"
								onClick={() => archiveToggle(product._id, product.isActive)}
							>
								Enable
							</Button>
						}
					</td>
				</tr>

			)

		});

		setProducts(productsArr);

	}, [productsData, fetchData]);

	return(
		<Fragment>

			<div className="text-center my-4">
				<h2>Admin Dashboard</h2>
				<div className="d-flex justify-content-center">
					<Button variant="primary" onClick={openAdd}>Add New Product</Button>			
				</div>			
			</div>

			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Quantity</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>					
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>

			{/*ADD MODAL*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={productName} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productQuantity">
							<Form.Label>Quantity</Form.Label>
							<Form.Control type="number" value={quantity}  onChange={e => setQuantity(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			{/*EDIT MODAL*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={productName} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productQuantity">
							<Form.Label>Quantity</Form.Label>
							<Form.Control type="number" value={quantity}  onChange={e => setQuantity(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
			
		</Fragment>
	)
}