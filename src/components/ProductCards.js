import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

	const {_id, productName, description, price} = productProp;

	 return (
        <Card className="banner">
            <Card.Body>
                <Card.Title>{productName}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Link className="btn btn-primary" to={`/product/${_id}`}>More Info</Link>
            </Card.Body>
        </Card>
    )

}