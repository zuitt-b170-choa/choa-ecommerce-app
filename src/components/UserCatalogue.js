import { Fragment, useState, useEffect } from 'react'
import ProductCards from "./ProductCards";

export default function UserCatalogue({productsData}){
	const [products,setProducts] = useState([]);

	useEffect(()=>{

		const productsArray = productsData.map(products=>{
			if(products.isActive === true){
				return (
					<ProductCards productProp={products} key={products.id}/>
					)
			}else{
				return null;
			}
		});
		setProducts(productsArray);
	}, [productsData]);

	return(
		<Fragment>
			{products}
		</Fragment>
	);
}