
import React from 'react';

import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Highlights() {
	return (
			<Row className = "cards mx-2">
				{/*xs and md - allows setting of breakpoints*/}
				<Col xs={12} md={4}>
					<Card className="card-highlight">
						<Card.Body>
							<Card.Title>
								<h2>Your favorite items right at your doorstep!</h2>
							</Card.Title>
							<Card.Text>
								Anything that you need we got you.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="card-highlight">
						<Card.Body>
							<Card.Title>
								<h2>Fast Order, Fast Delivery!</h2>
							</Card.Title>
							<Card.Text>
								With our very own logistics, ShoZada Delivery, we'll guarantee same day delivery.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="card-highlight">
						<Card.Body>
							<Card.Title>
								<h2>Always wanted to own your business?</h2>
							</Card.Title>
							<Card.Text>
								We offer services to help you set up your very own business.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		)
}