import React from 'react';
import {Fragment, useEffect,useState,useContext} from 'react';

import AdminCatalogue from "../components/AdminCatalogue";
import UserCatalogue from "../components/UserCatalogue";

import UserContext from '../userContext.js'

export default function Products(){

	const {user} = useContext(UserContext);

	const [products,setProducts] = useState([]);

	const fetchData = ()=>{
		fetch('https://immense-refuge-40846.herokuapp.com/api/products/')
		.then(res => res.json())
        .then(data => {
            setProducts(data);
        });

	}

	useEffect(()=>{
		fetchData();
	},[]);
	let catalogue = (user.isAdmin === true) ? (
		<AdminCatalogue productsData = {products} fetchData = {fetchData}/>
		) : (
		<UserCatalogue productsData = {products}/>
		)

	return(
		<Fragment>
			{catalogue}
		</Fragment>
	)
}