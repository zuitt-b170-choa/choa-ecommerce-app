import	React, { useState, useEffect, useContext } from 'react';
import	{ Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../userContext.js';
import { Form, Container, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Register(){
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [address, setAddress] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [classification, setClassification] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);
/*
	console.log(email);
	console.log(password1);
	console.log(password2);	*/

	function registerUser(e) {
	e.preventDefault();
	fetch('https://immense-refuge-40846.herokuapp.com/api/users/checkEmail', {
	    method: "POST",
	    headers: {
	        'Content-Type': 'application/json'
	    },
	    body: JSON.stringify({
	        email: email
	    })
	})
	.then(res => res.json())
	.then(data => {

	    console.log(data);

	    if(data === true){

	    	Swal.fire({
	    		title: 'Email already registered.',
	    		icon: 'error',
	    		text: 'Please use another email or login.'	
	    	});

	    }
	    else{
	    	e.preventDefault();
	    	fetch('https://immense-refuge-40846.herokuapp.com/api/users/register', {
	    		method: "POST",
	    		headers: {
	    		    'Content-Type': 'application/json'
	    		},
	    		body: JSON.stringify({
	    		    firstName: firstName,
	    		    lastName: lastName,
	    		    email: email,
	    		    address: address,
	    		    mobileNo: mobileNo,
	    		    password: password1,
	    		    classification: classification
	    		})

	    	})
	    	.then(res => res.json())
	    	.then(data => {
	    		console.log(data);

	    		if (data === true) {

	    		    // Clear input fields
	    		    setFirstName('');
	    		    setLastName('');
	    		    setEmail('');
	    		    setAddress('');
	    		    setMobileNo('');
	    		    setClassification('');
	    		    setPassword1('');
	    		    setPassword2('');

	    		    Swal.fire({
	    		        title: 'Registration successful',
	    		        icon: 'success',
	    		        text: 'Welcome to ShoZada!'
	    		    });

	    		    navigate("/login");

	    		} else {

	    		    Swal.fire({
	    		        title: 'Something wrong',
	    		        icon: 'error',
	    		        text: 'Please try again.'   
	    		    });

	    		}
	    	})
	    }


	})

}


	useEffect(() => {
		// Validation to enable the submit buttion when all fields are populated and both passwords match
		if((firstName !== '' && lastName !== '' && email !== '' && address !== '' && mobileNo.length === 11 && classification !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [firstName, lastName, email, address, mobileNo, classification, password1, password2])

	return (
		(user.id !== null) ?
		    <Navigate to="/products" />
		:
		<Container className="banner">
			<h1>Register</h1>
			<Form className="mt-3" onSubmit={(e) => registerUser(e)}>
				<Form.Group controlId="firstName">
				    <Form.Label>First Name</Form.Label>
				    <Form.Control 
				        type="text" 
				        placeholder="Enter first name"
				        value={firstName} 
				        onChange={e => setFirstName(e.target.value)}
				        required
				    />
				</Form.Group>

				<Form.Group controlId="lastName">
				    <Form.Label>Last Name</Form.Label>
				    <Form.Control 
				        type="text" 
				        placeholder="Enter last name"
				        value={lastName} 
				        onChange={e => setLastName(e.target.value)}
				        required
				    />
				</Form.Group>

			  <Form.Group className="mb-3" controlId="email">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value = {email}
			    	onChange = { e => setEmail(e.target.value)}
			    	required 
			    />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="userAddress">
			    <Form.Label>Home Address</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter Home Address" 
			    	value = {address}
			    	onChange = { e => setAddress(e.target.value)}
			    	required 
			    />
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="mobileNo">
			      <Form.Label>Mobile Number</Form.Label>
			      <Form.Control 
			          type="text" 
			          placeholder="Enter Mobile Number"
			          value={mobileNo} 
			          onChange={e => setMobileNo(e.target.value)}
			          required
			      />
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="classification">
			    <Form.Label>Classification</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter Classification (ex. Seller, User)" 
			    	value = {classification}
			    	onChange = { e => setClassification(e.target.value)}
			    	required 
			    />
			  </Form.Group>


			  <Form.Group className="mb-3" controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password1}
			    	onChange = { e => setPassword1(e.target.value)}
			    	required 
			    />
			  </Form.Group>
			  
			  <Form.Group className="mb-3" controlId="password2">
			    <Form.Label>Verify Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Verify Password" 
			    	value={password2}
			    	onChange = { e => setPassword2(e.target.value)}
			    	required 
			    />
			  </Form.Group>
			{/* Conditionally render submit button based on isActive state */}
			  {(isActive) ? 
			  		<Button variant="primary" type="submit" id="submitBtn">
			  		  Submit
			  		</Button>
			  	:
				  	<Button variant="primary" type="submit" id="submitBtn" disabled>
				  	  Submit
				  	</Button>
			  }
			  
			</Form>
		</Container>
	)
}