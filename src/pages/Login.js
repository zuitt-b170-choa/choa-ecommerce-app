import React, { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';

import UserContext from '../userContext'

import { Form, Container, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Login(){

	const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [ isDisabled, setIsDisabled ] = useState(true);

    const navigate = useNavigate();

    function login(e) {
        e.preventDefault();

        fetch('https://immense-refuge-40846.herokuapp.com/api/users/login', {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify( {
                email: email, 
                password: password
                } )
        })
        .then((response) => response.json())
        .then((response) => {
            if (typeof response.access !== "undefined") {
                Swal.fire('You are logged in')
                // localStorage.setItem - to store a piece of data inside the localStorage of the browser. this is because the local storage does not delete unless it manually done so or the codes make it delete the information inside
                localStorage.setItem('access', response.access);
                retrieveUserDetails(response.access)
            }
            else {
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again."
                });
            }
        })
        setEmail('');
        setPassword('');
    }    

    const retrieveUserDetails = (access) =>{
        fetch(`https://immense-refuge-40846.herokuapp.com/api/users/details`,{
            headers: {
                Authorization: `Bearer ${access}`
            }
        }).then(res =>res.json())
        .then(data=>{
            console.log(data);
            console.log(data._id)
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(()=>{
        if(email !== '' && password !== ''){
            setIsDisabled(true);
        }else{
            setIsDisabled(false);
        }
    },[email,password]);
    

    return (
        (user.id !== null) ?
            <Navigate to="/products"/>
        :
        <Container fluid className="banner">
            <h3>Login</h3>

            <Form onSubmit={login}>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required/>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)} 
                        required/>
                </Form.Group>
                <br></br>

                {isDisabled ?
                    <Button variant="primary" type="submit" id="submitBtn">Login</Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" disabled>Login</Button>
            }
            </Form>
        </Container>
    )
}
