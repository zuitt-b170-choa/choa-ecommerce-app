import	React, { Fragment } from 'react';

import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';

export default function Home() {

	const data = {
	    title: "ShoZada",
	    content: "Your One-Stop E-Shop",
	    destination: "/products",
	    label: "Buy now!"
	}

	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}